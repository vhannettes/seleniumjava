package seleniumframework.pageobjects.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPageLocators {

	public static class TextField {

		public static By username = By.id("user-name");
		public static By password = By.id("password");
		public static By errMessage = By.cssSelector("h3[data-test='error']");

	}

	public static class Button {

		public static By login = By.id("login-button");

	}
}
