package basetest;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;



public class BaseTest {

	public static WebDriver driver;
	Properties prop = new Properties();




	@BeforeTest
	public void testSetUp() throws IOException
	{

		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
				+ "//src//test//resources//configfiles//config.properties");
		prop.load(fis);

		String browserName = System.getProperty("browser")!=null ? System.getProperty("browser") : prop.getProperty("browser");
		//prop.getProperty("browser");

		if (browserName.contains("chrome")) {
			// chrome
			// options = new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.get("https://www.saucedemo.com");


		} else if (browserName.equalsIgnoreCase("firefox")) {
			// Firefox

			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			driver.get("https://www.saucedemo.com");


		} else if (browserName.equalsIgnoreCase("edge")) {
			// Edge

			WebDriverManager.chromedriver().setup();
			driver = new EdgeDriver();
			driver.get("https://www.saucedemo.com");

		}

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.manage().window().maximize();


	}



	@AfterTest
	public void testTearDown() {
		driver.close();

	}


}
